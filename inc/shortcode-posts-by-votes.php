<?php

add_action( 'vc_before_init', function() {

	$categories = get_terms( [
		'taxonomy' => 'category'
		, 'fields' => 'id=>name'
		, 'hide_empty' => false
	] );

	$values = [];

	foreach ( $categories as $id => $name )	{
		$values[] = [ 'value' => $id, 'label' => $name ];
	}

	vc_map( [
		'name' => 'Posts by Votes',
		'base' => 'posts_by_votes',
		'category' => 'Tweaks by NK',
		'params' => [
			[
				'type' => 'textfield'
				, 'param_name' => 'numberposts'
				, 'heading' => 'Posts to show'
				, 'description' => 'Numbers of posts to show'
				, 'value' => '3'
			],
			[
				'type' => 'textfield'
				, 'param_name' => 'offset'
				, 'heading' => 'Offset'
				, 'description' => 'Numbers of posts to skip'
				, 'value' => '0'
			],
			[
				'type' => 'textfield'
				, 'param_name' => 'words'
				, 'heading' => 'Words'
				, 'description' => 'Numbers of words to show'
				, 'value' => '10'
			],
			[
				'type' => 'autocomplete'
				, 'param_name' => 'categories'
				, 'heading' => 'Categories'
				, 'description' => 'Leave empty to use all categories'
				, 'settings' => [
					'multiple' => true
					, 'display_inline' => true
					, 'unique_values' => true
					, 'values' => $values
				]
			],
			[
				'type' => 'dropdown'
				, 'param_name' => 'highlight'
				, 'heading' => 'Highlight top 3'
				, 'description' => 'This will give a special look to the first 3 items'
				, 'value' => [
					'No' => 'no'
					, 'Yes' => 'yes',
				]
			],
			[
				'type' => 'dropdown'
				, 'param_name' => 'average'
				, 'heading' => 'Source'
				, 'description' => 'Where to get average from'
				, 'value' => [
					'Multisets' => 'multiset'
					, 'Basic votes' => 'basic'
				]
			],
		]
	] );
} );

add_shortcode( 'posts_by_votes', function( $atts, $content = null ) {

	$force_rebuild = current_user_can( 'manage_options' ) && ! empty( $_REQUEST[ 'force_rebuild' ] );

	/**
	 * Grab shortcode attributes into variables
	 *   - $numberposts
	 *   - $words
	 *   - $offset
	 *   - $categories
	 */
	extract( shortcode_atts( array(
		'numberposts' => 3,
		'words' => 10,
		'offset' => 0,
		'categories' => '',
		'average' => 'multiset',
		'highlight' => ''
	), $atts ) );

	/**
	 * Turn $highlignt into bolean.
	 */
	$highlight = ( 'yes' === $highlight ) ? true : false;

	/**
	 * Prevent server overload by accepting only the range 1-100
	 */
	$numberposts = ! empty( $numberposts )
		? min( max( intval( $numberposts ), 1 ), 100 )
		: 12
	;

	/**
	 * Prepare array of categories ids.
	 */
	preg_match_all( '/\d+/', $categories, $match );
	$category__in = end( $match );

	/**
	 * Get matching posts.
	 */
	switch ( $average ) {
	case 'basic':
		$meta_key = 'codeable_42265_yasr_votes_average';
		break;
	case 'multiset':
	default:
		$meta_key = 'codeable_42265_yasr_multisets_average';
		break;
	}

	$posts = get_posts( [
		// 'post_type' => 'any',
		'numberposts' => $numberposts,
		'category__in' => $category__in,
		'offset' => $offset,
		'orderby' => 'meta_value_num',
		'meta_key' => $meta_key,
		'order' => 'desc',
	] );

	/**
	 * The post HTML template.
	 */
	$template =
		'
			<div class="vc_col-sm-{{col}} codeable_42265_post_by_votes_column codeable_42265_post_by_votes_column_{{i}}" >
				{{rank_box}}
				[vc_single_image image="{{attachment_id}}" img_size="200x200" alignment="center" style="vc_box_circle" css_animation="top-to-bottom"]
				[vc_custom_heading
					text="{{title}}"
					font_container="tag:h4|font_size:22|text_align:center|color:%233a3a3a|line_height:1.2" 
					google_fonts="font_family:Montserrat%3Aregular%2C700|font_style:400%20regular%3A400%3Anormal"
				]
				[vc_column_text css_animation="appear"]
					<div style="text-align: center;">{{votes}}</div>
				[/vc_column_text]
				[vc_column_text css_animation="left-to-right"]
					{{excerpt}}
				[/vc_column_text]
				[vc_btn title="Read More"
					style="flat"
					shape="round"
					color="primary"
					align="center"
					css_animation="appear"
					link="url:{{permalink}}"
				]
			</div>
		'
	;

	/**
	 * Build output.
	 */
	$output = '';

	/**
	 * only 3 columns per row
	 * @var integer
	 */
	$i = 0;
	$output .= '<div style="clear:both;">';

	foreach ( $posts as $post ) {

		$cache_transient_name = sprintf( 'codeable_42265_posts_by_votes_%s_%s_%s'
			, base64_encode( var_export( [ $words, $average ], true ) )
			, $post->ID
			, $i
		);

		/**
		 * Check cache.
		 */
		if ( ! $force_rebuild ) {
			$cached = get_transient( $cache_transient_name );

			if ( false !== $cached ) {
				$i++;
				$output .= $cached;
				continue;
			}
		}

		/**
		 * No chache found, let's rebuild.
		 */

		$sub_output = '';

		if ( $i && ( $i % 3 === 0 ) ) {
			$sub_output .= '</div><div style="clear:both;">';
		}

		/**
		 * Get featured or first image id.
		 */
		$attachment_id = get_post_thumbnail_id( $post->ID );
		// if ( ! $attachment_id ) {
		// 	$images =& get_children( 'post_type=attachment&post_mime_type=image&post_parent=' . $post->ID );
		// 	$attachment_id = array_shift( array_keys( $images ) );
		// }

		if( ! $attachment_id ) {
			// ffim_set_featured_img( $post->ID );
		}

		/**
		 * Votes average.
		 */
		$stars_attribute = yasr_stars_size( 'large' );
		switch ( $average ) {
		case 'basic':
			$value = get_post_meta( $post->ID, 'codeable_42265_yasr_votes_average', true );
			break;
		case 'multiset':
			$value = get_post_meta( $post->ID, 'codeable_42265_yasr_multisets_average', true );
			break;
		default:
			$value = 0;
			break;
		}

		$votes = "<div
			class=\"$stars_attribute[class]\"
			id=\"yasr_rateit_visitor_votes_readonly_{$post->ID}\"
			data-rateit-starwidth=\"$stars_attribute[px_size]\"
			data-rateit-starheight=\"$stars_attribute[px_size]\"
			data-rateit-value=\"$value\"
			data-rateit-step=\"1\"
			data-rateit-resetable=\"false\"
			data-rateit-readonly=\"true\"></div>"
		;

		/**
		 * Highlight.
		 */
		$col = 4;
		$rank_box = '';
		if ( $highlight ) {
			$suffixes = [ 'th','st','nd','rd','th','th','th','th','th','th' ];
			$rank = $i + 1;
			if ( ( $rank % 100 ) >= 11 && ( $rank % 100 ) <= 13 ) {
				$rank_with_suffix = $rank . 'th';
			} else {
				$rank_with_suffix = $rank . $suffixes[ $rank % 10 ];
			}
			$rank_box = sprintf(
				'
					<div class="codeable_42265_post_by_votes_rank_wrapper">
						<div class="codeable_42265_post_by_votes_ranked">Ranked</div>
						<div class="codeable_42265_post_by_votes_rank codeable_42265_post_by_votes_rank_%s">%s</div>
					</div>
				'
				, $rank
				, $rank_with_suffix
			);
			switch ( $i )  {
			case 0:
				$col = 12;
				break;
			case 1:
			case 2:
				$col = 6;
				break;
			default:
				$rank_box = '';
			}
		}

		/**
		 * Replace placeholders.
		 */
		$replacements = [
			'{{title}}'         => $post->post_title,
			'{{attachment_id}}' => $attachment_id,
			'{{permalink}}'     => urlencode( get_permalink( $post->ID ) ),
			'{{excerpt}}'       => $post->post_excerpt,
			'{{votes}}'         => $votes,
			'{{rank_box}}'      => $rank_box,
			'{{col}}'           => $col,
			'{{i}}'             => $i,
		];

		$sub_output .= str_replace(
			array_keys( $replacements ),
			array_values( $replacements ),
			$template
		);

		$output .= $sub_output;

		$i++;

		/**
		 * Store to cache.
		 */
		set_transient( $cache_transient_name, $sub_output, DAY_IN_SECONDS );
	}

	$output .= '</div>';

	$output = sprintf( '<div class="shortcode_codeable_42265_posts_by_votes">%s</div>', $output );

	/**
	 * That's all.
	 */
	return do_shortcode( $output );
} );
