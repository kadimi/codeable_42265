<?php

/**
 * Register widget.
 */
add_action( 'widgets_init', function() {
	register_widget( 'Codeable_42265_Posts_By_Votes_Widget' );
} );

/**
 * Adds Codeable_42265_Posts_By_Votes_Widget widget.
 */
class Codeable_42265_Posts_By_Votes_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct( 'Codeable_42265_Posts_By_Votes_Widget', 'Tweaks by NK - Posts by votes', [ 'description' => '' ] );
	}

	/**
	 * Front-end display of widget.
	 */
	public function widget( $args, $instance ) {

		/**
		 * Open widget and show title.
		 */
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}

		/**
		 * Show content.
		 */
		$posts = get_posts( [
			'post_type' => 'any',
			'numberposts' => $instance['numberposts'],
			'category__in' => $instance['category__in'],
			'orderby' => 'meta_value_num',
			'meta_key' => ( $instance[ 'average'] === 'basic' )
				? 'codeable_42265_yasr_votes_average'
				: 'codeable_42265_yasr_multisets_average'
			,
			'order' => 'DESC',
		] );
		echo '<ul>';
		array_walk( $posts, function( $post, $i ) use ( $instance ) {

			$featured_image = get_the_post_thumbnail( $post, 'medium' );

			if( ! $featured_image ) {
				ffim_set_featured_img( $post->ID );
				$featured_image = get_the_post_thumbnail( $post, 'medium' );
			}

			/**
			 * Turn highlight into a boolean.
			 */
			$instance[ 'highlight' ] = 'yes' === $instance[ 'highlight' ];

			$stars_attribute = yasr_stars_size( K::get_var( 'size', $instance, '' ) );
			switch ( $instance[ 'average' ] ) {
			case 'basic':
				$value = round( ( float ) get_post_meta( $post->ID, 'codeable_42265_yasr_votes_average', true ), 2 );
				$count = ( int ) get_post_meta( $post->ID, 'codeable_42265_yasr_votes_count', true );
				break;
			case 'multiset':
			default:
				$value = round( ( float ) get_post_meta( $post->ID, 'codeable_42265_yasr_multisets_average', true ), 2 );
				$count = get_post_meta( $post->ID, 'codeable_42265_yasr_multisets_count', true );
				break;
			}

			$votes = "<div
				class=\"$stars_attribute[class]\"
				id=\"yasr_rateit_visitor_votes_readonly_$post->ID\"
				data-rateit-starwidth=\"$stars_attribute[px_size]\"
				data-rateit-starheight=\"$stars_attribute[px_size]\"
				data-rateit-value=\"$value\"
				data-rateit-step=\"1\"
				data-rateit-resetable=\"false\"
				data-rateit-readonly=\"true\"></div>"
			;

			/**
			 * Prepend rank if applicable;
			 */
			if ( $instance[ 'highlight' ] ) {
				$suffixes = [ 'th','st','nd','rd','th','th','th','th','th','th' ];
				$rank = $i + 1;
				if ( ( $rank % 100 ) >= 11 && ( $rank % 100 ) <= 13 ) {
					$rank_with_suffix = $rank . 'th';
				} else {
					$rank_with_suffix = $rank . $suffixes[ $rank % 10 ];
				}

				$rank_box = sprintf( '<div class="codeable_42265_post_by_votes_ranked">Ranked</div><div class="codeable_42265_post_by_votes_rank codeable_42265_post_by_votes_rank_%s">%s</div>'
					, $rank
					, $rank_with_suffix
				);

				$votes = $rank_box . $votes;
			}

			if ( $instance[ 'show_stats' ] ) {
				$votes .= '<br>'
					. '<span style="color:#a9a9a9">'
					. '<span class="dashicons dashicons-chart-bar"></span>' 
					. " [Total: $count &nbsp; &nbsp; Average: $value/5]" 
					// . sprintf( _n( '%s vote', '%s votes', $count ), $count )
					. '</span>'; 
			}

			printf( '<li><a href="%s">%s</a>%s<br>%s</li>'
				, get_permalink( $post->ID )
				, $post->post_title
				, $featured_image
				, $instance[ 'size' ]
					? $votes
					: ''
			);
		} );
		echo '</ul>';

		/**
		 * Close widget.
		 */
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 */
	public function form( $instance ) {

		/**
		 * Title field
		 */
		$title = K::get_var( 'title', $instance, 'Best voted posts' );
		K::input( esc_attr( $this->get_field_name( 'title' ) )
			, [
				'id' => esc_attr( $this->get_field_id( 'title' ) ),
				'class' => 'widefat',
				'value' => esc_attr( $title ),
			]
			, [
				'format' => sprintf( '<p><label for="%s">%s :input</label></p>'
					, esc_attr( $this->get_field_id( 'title' ) )
					, 'Title:'
				),
			]
		);

		/**
		 * Number field
		 */
		$numberposts = ( int ) K::get_var( 'numberposts', $instance, 5 );
		K::input( esc_attr( $this->get_field_name( 'numberposts' ) )
			, [
				'type' => 'number',
				'id' => esc_attr( $this->get_field_id( 'numberposts' ) ),
				'class' => 'widefat',
				'value' => $numberposts,
				'min' => 1,
				'max' => 50,
			]
			, [
				'format' => sprintf( '<p><label for="%s">%s :input</label></p>'
					, esc_attr( $this->get_field_id( 'numberposts' ) )
					, 'Number of posts to show:'
				),
			]
		);

		/**
		 * Categpries
		 */
		$category__in = K::get_var( 'category__in', $instance, 5 );
		K::select( esc_attr( $this->get_field_name( 'category__in' ) )
			, [
				'id' => esc_attr( $this->get_field_id( 'category__in' ) ),
				'class' => 'widefat',
				'multiple' => true,
			]
			, [
				'options' => get_terms( [
					'taxonomy' => 'category',
					'hide_empty' => false,
					'fields' => 'id=>name',
				] ),
				'format' => sprintf( '<p><label for="%s">%s :select</label></p>'
					, esc_attr( $this->get_field_id( 'category__in' ) )
					, 'Categories:'
				),
				'selected' => $category__in,
			]
		);

		/**
		 * Highlight
		 */
		$highlight = K::get_var( 'highlight', $instance, 'no' );
		K::select( esc_attr( $this->get_field_name( 'highlight' ) )
			, [
				'id' => esc_attr( $this->get_field_id( 'highlight' ) ),
				'class' => 'widefat',
			]
			, [
				'options' => [
					'no' => 'No',
					'yes' => 'Yes',
				],
				'format' => sprintf( '<p><label for="%s">%s :select</label></p>'
					, esc_attr( $this->get_field_id( 'highlight' ) )
					, 'Highlight:'
				),
				'selected' => $highlight,
			]
		);

		/**
		 * Source
		 */
		$average = K::get_var( 'average', $instance, 'multiset' );
		K::select( esc_attr( $this->get_field_name( 'average' ) )
			, [
				'id' => esc_attr( $this->get_field_id( 'average' ) ),
				'class' => 'widefat',
			]
			, [
				'options' => [
					'multiset' => 'Multisets',
					'basic' => 'Basic votes',
				],
				'format' => sprintf( '<p><label for="%s">%s :select</label></p>'
					, esc_attr( $this->get_field_id( 'average' ) )
					, 'Source:'
				),
				'selected' => $average,
			]
		);

		/**
		 * Size
		 */
		$size = K::get_var( 'size', $instance, 'small' );
		K::select( esc_attr( $this->get_field_name( 'size' ) )
			, [
				'id' => esc_attr( $this->get_field_id( 'size' ) ),
				'class' => 'widefat',
			]
			, [
				'options' => [
					'' => '- None -',
					'small' => 'Small',
					'medium' => 'Medium',
					'large' => 'Large',
				],
				'format' => sprintf( '<p><label for="%s">%s :select</label></p>'
					, esc_attr( $this->get_field_id( 'size' ) )
					, 'Stars:'
				),
				'selected' => $size,
			]
		);

		/**
		 * Stats
		 */
		$show_stats = K::get_var( 'show_stats', $instance, '' );
		K::select( esc_attr( $this->get_field_name( 'show_stats' ) )
			, [
				'id' => esc_attr( $this->get_field_id( 'show_stats' ) ),
				'class' => 'widefat',
			]
			, [
				'options' => [
					'0' => 'No',
					'1' => 'Yes',
				],
				'format' => sprintf( '<p><label for="%s">%s :select</label></p>'
					, esc_attr( $this->get_field_id( 'show_stats' ) )
					, 'Show stats:'
				),
				'selected' => $show_stats,
			]
		);
	}

	/**
	 * Sanitize widget form values as they are saved.
	 */
	public function update( $new_instance, $old_instance ) {
		return [
			'title'        => strip_tags( K::get_var( 'title', $new_instance, '' ) ),
			'numberposts'  => ( int ) K::get_var( 'numberposts', $new_instance, 5 ),
			'category__in' => K::get_var( 'category__in', $new_instance, [] ),
			'highlight'    => in_array( K::get_var( 'highlight', $new_instance ), [ 'no', 'yes' ] )
				? K::get_var( 'highlight', $new_instance )
				: 'no'
			,
			'average'      => in_array( K::get_var( 'average', $new_instance ), [ 'multiset', 'basic' ] )
				? K::get_var( 'average', $new_instance )
				: 'multiset'
			,
			'size' => in_array( K::get_var( 'size', $new_instance ), [ 'small', 'medium', 'large' ] )
				? K::get_var( 'size', $new_instance )
				: ''
			,
			'show_stats' => K::get_var( 'show_stats', $new_instance, '' ),
		];
	}
}
