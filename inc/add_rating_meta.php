<?php

/**
 * Update votes.
 */
add_action( 'init', function() {

	$force_update = current_user_can( 'manage_options' ) && ! empty( $_REQUEST[ 'force_update' ] );

	/**
	 * Check transient.
	 */
	if ( ! $force_update && get_transient( 'codeable_42265_yasr_votes_meta_updated' ) ) {
		return;
	}

	/**
	 * Save transient.
	 */
	set_transient( 'codeable_42265_yasr_votes_meta_updated', 1, 6 * HOUR_IN_SECONDS );

	$posts = get_posts( [
		'post_type' => 'any',
		'meta_query' => $force_update
			? null
			: [ [ 'key' => 'codeable_42265_yasr_votes_average', 'compare' => 'NOT EXISTS' ] ]
		,
		'numberposts' => 100,
		'orderby' => 'rand',
	] );

	$t = microtime();

	array_walk( $posts, function( $post ) {

		/**
		 * Update basic votes stats.
		 */
		$votes = end( yasr_get_visitor_votes( $post->ID ) );
		$count = $votes->number_of_votes;
		$average = $count
			? $votes->sum_votes / $votes->number_of_votes
			: 0
		;
		update_post_meta( $post->ID, 'codeable_42265_yasr_votes_average', $average );
		update_post_meta( $post->ID, 'codeable_42265_yasr_votes_count', $count );

		/**
		 * Update multiset votes stats.
		 */
		global $wpdb;
		$multisets_votes = end( $wpdb->get_results( $wpdb->prepare( '
			SELECT SUM(sum_votes) as sum_votes, SUM(number_of_votes) as number_of_votes
			FROM ' . YASR_MULTI_SET_VALUES_TABLE . '
			WHERE post_id=%d',
			$post->ID
		) ) );
		$count = $multisets_votes->sum_votes
			? $multisets_votes->sum_votes
			: 0
		;
		$average = $count > 0
			? $multisets_votes->sum_votes / $multisets_votes->number_of_votes
			: 0
		;
		// ! d($count, $average);
		update_post_meta( $post->ID, 'codeable_42265_yasr_multisets_average', $average );
		update_post_meta( $post->ID, 'codeable_42265_yasr_multisets_count', $count );
		update_post_meta( $post->ID, 'yasr_overall_rating', $average );
	} );

	if ( $force_update ) {
		$seconds = ( microtime() - $t ) / 1000;
		ddd( sprintf( '%d posts updates in %f seconds', count( $posts ), $seconds ) );
	}

} );

/**
 * Force recalculating votes
 */
add_action( 'yasr_action_on_visitor_vote' ,                    'codeable_42265_reset_votes');
add_action( 'wp_ajax_yasr_visitor_multiset_field_vote',        'codeable_42265_reset_votes_from_post', 1 );
add_action ('wp_ajax_nopriv_yasr_visitor_multiset_field_vote', 'codeable_42265_reset_votes_from_post', 1 );

function codeable_42265_reset_votes( $post_id ) {
	delete_post_meta( $post_id, 'codeable_42265_yasr_votes_average' );
	delete_post_meta( $post_id, 'codeable_42265_yasr_votes_count' );
	delete_post_meta( $post_id, 'codeable_42265_yasr_multisets_average' );
	delete_transient( 'codeable_42265_yasr_votes_meta_updated' );
}

function codeable_42265_reset_votes_from_post() {
	$post_id = K::get_var( 'post_id', $_POST );
	if ( $post_id ) {
		codeable_42265_reset_votes( $post_id );
	}
}