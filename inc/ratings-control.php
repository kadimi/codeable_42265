<?php

add_action( 'admin_enqueue_scripts', function() {
	wp_enqueue_script( 'datatables', 'https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js', [ 'jquery' ] );
	wp_enqueue_style( 'datatables', 'https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css' );
} );

add_action( 'add_meta_boxes', function() {
	add_meta_box( 'ratings_control', __( 'Ratings Control' ), function() {

		global $wpdb;
		$query = 'SELECT * FROM ' . YASR_LOG_TABLE . ' WHERE post_id=%d ORDER BY id DESC LIMIT 1000';
		$post_id = get_the_id();

		$votes = $wpdb->get_results( $wpdb->prepare( $query , $post_id ) );

		echo
			'<table id="codeable_42265_ratings_table" class="display">
				<thead>
					<tr>
						<th>ID</th>
						<th>Vote</th>
						<th>User</th>
						<th>Date and Time</th>
						<th>Country</th>
						<th>IP</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
			'
		;
		array_walk( $votes, function( $vote ) use ( $post_id ) {
			$date = date_i18n( get_option( 'date_format' ) . ' ' . get_option( 'time_format' ), strtotime( $vote->date ) );
			$user = $vote->user_id
				? sprintf( '%s (#%d)', esc_attr( get_userdata( $vote->user_id )->user_nicename ), $vote->user_id )
				: '- Guest -'
			;

			echo
				'<tr>
					<td>' . $vote->id . '</td>
					<td>' . $vote->vote . '</td>
					<td>' . $user . '</td>
					<td>' . $date . '</td>
					<td>' . ( function_exists( 'geoip_detect2_get_info_from_ip' ) ? geoip_detect2_get_info_from_ip( $vote->ip, null )->country->name : 'Plugin "GeoIP Detection" not installed' ) . '</td>
					<td>' . $vote->ip . '</td>
					<td>
						<span class="dashicons dashicons-trash"></span>
						<a
							class="codeable_ajax_42265_link"
							href="javascript:void(0)"
							data-codeable-ajax-param-action="codeable_delete_42265_rating"
							data-codeable-ajax-param-id="' . $vote->id . '"
						>Delete</a>
					</td>
				</tr>'
			;
		} );
		echo '</tbody></table>';
	}, null, 'normal' );
} );

add_action( 'wp_ajax_codeable_delete_42265_rating', function() {

	global $wpdb;
	$id = ! empty( $_REQUEST[ 'id' ] )
		? $_REQUEST[ 'id' ]
		: 0
	;

	/**
	 * Get and delete transient
	 */
	$post_id = $wpdb->get_var( $wpdb->prepare( "SELECT post_id FROM {$wpdb->prefix}yasr_log WHERE id = %d LIMIT 1", $id ) );
	delete_transient( 'yasr_visitor_votes_' . $post_id );
	delete_transient( 'yasr_overall_rating_' . $post_id );

	/**
	 * Delete rating
	 */
	echo $wpdb->delete( YASR_LOG_TABLE
		, [ 'id' => $id ]
		, [ '%d' ]
	);

	wp_die();
} );

add_filter( 'manage_posts_columns', 'codeable_42265_manage_columns' );
add_filter( 'manage_pages_columns', 'codeable_42265_manage_columns' );
function codeable_42265_manage_columns( $columns ) {
	return array_merge( $columns, [ 'votes' => 'Votes' ] );
};

add_action( 'manage_posts_custom_column' , 'codeable_42265_manage_custom_column', 10, 2 );
add_action( 'manage_pages_custom_column' , 'codeable_42265_manage_custom_column', 10, 2 );
function codeable_42265_manage_custom_column( $column, $post_id ) {
	switch ( $column ) {
		case 'votes':

			$average = get_post_meta( $post_id, 'codeable_42265_yasr_votes_average', true );
			$count = get_post_meta( $post_id, 'codeable_42265_yasr_votes_count', true );

			printf( '%.2g (%d votes)', $average, $count );
	}
}

add_filter( 'manage_edit-post_sortable_columns', 'codeable_42265_manage_sortable_columns' );
add_filter( 'manage_edit-page_sortable_columns', 'codeable_42265_manage_sortable_columns' );
function codeable_42265_manage_sortable_columns( $sortable_columns ) {
	return array_merge( $sortable_columns, [ 'votes' => [ 'votes', true ] ] );
}

add_filter( 'request', function( $vars ) {
	if ( ! empty( $vars['orderby'] ) && 'votes' == $vars[ 'orderby' ] ) {
		$vars = array_merge( $vars, [
			'meta_key' => 'codeable_42265_yasr_votes_average',
			'orderby' => 'meta_value_num',
		] );
	}
	return $vars;
} );
