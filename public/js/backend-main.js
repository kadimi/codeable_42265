jQuery( document ).ready( function( $ ) {

	$( '#codeable_42265_ratings_table' ).DataTable();

	$( '.codeable_ajax_42265_link' ).on( 'click', function() {

		var $this = $( this );
		var params = $this.data();
		var data = {};
		var arg;
		var lowerFirst = function( str ) {
			return str.charAt(0).toLowerCase() + str.slice(1);
		};

		/**
		 * Prepare request parameters `data`
		 */
		for ( var param in params ) {
			if ( params.hasOwnProperty( param ) ) {
				if ( param.length > 'codeableAjaxParam'.length && param.indexOf( 'codeableAjaxParam' ) === 0 ) {
					arg = lowerFirst( param.replace( 'codeableAjaxParam', '' ) );
					data[ arg ] = params[ param ];
				}
			}
		}

		/**
		 * Send AJAX request
		 */
		switch ( data.action ) {
			/**
			 * codeable_delete_42265_rating
			 * - Highlight row
			 * - Ask to confirm deletion
			 * - Delete or unhighlight
			 */
			case 'codeable_delete_42265_rating' :
				$this.parents('tr').find( 'td' )
					.css( 'background', '#990012' )
					.css( 'color', '#fff' )
				;
				if ( confirm( 'You are about to delete the highlighted vote.' ) ) {
					$.post( ajaxurl, data, function( res ) {
						if ( res > 0) {
							$( '#codeable_42265_ratings_table' )
								.DataTable()
								.row( $( '[data-codeable-ajax-param-id=' + data.id + ']' ).parents( 'tr' ) )
								.remove()
								.draw()
							;
						}
					} );
				} else {
					$this.parents('tr').find( 'td' )
						.css( 'color', '' )
						.css( 'background', '' )
					;
				}
			/**
			 * All other actions
			 */
			default:
				console.log( 'Action not supported: ' + data.action );
		}
	} );
} );
